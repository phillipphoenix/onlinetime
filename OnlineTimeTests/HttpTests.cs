﻿using System;
using System.CodeDom;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineTimeUtil;

namespace OnlineTimeTests
{
    [TestClass]
    public class HttpTests
    {
        private const string TimeServerEndpoint = "https://us-central1-bowlingbuddies-5c0f5.cloudfunctions.net/time";

        [TestMethod]
        public void TestTestTest()
        {
            var dateTimeString = "2018-11-06T14:29:42.790+00:00";
            var dateTimeFormat = @"yyyy-MM-dd\THH:mm:ss.fffzzz";

            Console.WriteLine($"DateTimeString: {dateTimeString}.");
            Console.WriteLine($"DateTimeFormat: {dateTimeFormat}.");

            var dateTime = DateTime.ParseExact(dateTimeString, dateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            
            Console.WriteLine($"DateTime: {dateTime}");
        }

        [TestMethod]
        public void GetTimeGetTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter
                .SetFetchMethod(FetchMethod.Http)
                .SetHttpEndpoint(TimeServerEndpoint)
                .SetHttpMethod(HttpMethod.Get)
                .SetHttpSupportsFormatRequest(true)
                .SetUseCache(false);

            var dateTime = OnlineTime.GetTime();
            Console.WriteLine($"DateTime is: {dateTime}.");   
        }
        
        [TestMethod]
        [ExpectedException(typeof(NoConnectionException))]
        public void GetTimeFaultyEndpointTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter
                .SetFetchMethod(FetchMethod.Http)
                .SetHttpEndpoint("12345678910") // Some incorrect URL.
                .SetHttpMethod(HttpMethod.Get)
                .SetHttpSupportsFormatRequest(true)
                .SetUseCache(false);

            var dateTime = OnlineTime.GetTime();
            Console.WriteLine($"DateTime is: {dateTime}.");
        }

        [TestMethod]
        public void GetTimeGetLocalTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter
                .SetFetchMethod(FetchMethod.Http)
                .SetHttpEndpoint(TimeServerEndpoint)
                .SetHttpMethod(HttpMethod.Get)
                .SetHttpSupportsFormatRequest(true)
                .SetUseCache(false);

            var dateTime = OnlineTime.GetTime(true);
            Console.WriteLine($"DateTime is: {dateTime}.");
        }

        [TestMethod]
        public void GetTimeGetCachedTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter
                .SetFetchMethod(FetchMethod.Http)
                .SetHttpEndpoint(TimeServerEndpoint)
                .SetHttpMethod(HttpMethod.Get)
                .SetHttpSupportsFormatRequest(true)
                .SetUseCache(true);

            OnlineTime.ClearCache();
            
            var firstDateTime = OnlineTime.GetTime();

            Thread.Sleep(1000);

            var secondDateTime = OnlineTime.GetTime();

            TimeSpan diff = secondDateTime - firstDateTime;

            Assert.IsTrue(diff.TotalMilliseconds > 950 && diff.TotalMilliseconds < 1050);

            // Test caching speed.

            OnlineTime.ClearCache();
            Stopwatch watch = new Stopwatch();
            var firstDateTimeSpeedTest = OnlineTime.GetTime();
            Console.WriteLine($"Ticks without cache: {watch.ElapsedTicks}.");
            watch.Restart();
            var secondDateTimeSpeedTest = OnlineTime.GetTime();
            Console.WriteLine($"Ticks with cache: {watch.ElapsedTicks}.");
        }

        [TestMethod]
        public void GetTimePostTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter
                .SetFetchMethod(FetchMethod.Http)
                .SetHttpEndpoint(TimeServerEndpoint)
                .SetHttpMethod(HttpMethod.Post)
                .SetHttpSupportsFormatRequest(true)
                .SetUseCache(false);

            var dateTime = OnlineTime.GetTime();
            Console.WriteLine($"DateTime is: {dateTime}.");
        }

        [TestMethod]
        public void GetTimePostLocalTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter
                .SetFetchMethod(FetchMethod.Http)
                .SetHttpEndpoint(TimeServerEndpoint)
                .SetHttpMethod(HttpMethod.Post)
                .SetHttpSupportsFormatRequest(true)
                .SetUseCache(false);

            var dateTime = OnlineTime.GetTime(true);
            Console.WriteLine($"DateTime is: {dateTime}.");
        }
    }
}
