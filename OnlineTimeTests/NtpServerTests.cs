﻿using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineTimeUtil;

namespace OnlineTimeTests
{
    [TestClass]
    public class NtpServerTests
    {
        [TestMethod]
        public void GetTimeTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter.SetUseCache(false);

            var dateTime = OnlineTime.GetTime();
            Console.WriteLine($"DateTime is: {dateTime}.");
            
        }

        [TestMethod]
        public void GetTimeLocalTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter.SetUseCache(false);

            var dateTime = OnlineTime.GetTime(true);
            Console.WriteLine($"DateTime is: {dateTime}.");
        }

        [TestMethod]
        public void GetTimeCachedTest()
        {
            OnlineTimeSettings.RevertToDefaultSettings();
            OnlineTimeSettings.FluidSetter.SetUseCache(true);
            
            OnlineTime.ClearCache();
            
            var firstDateTime = OnlineTime.GetTime();

            Thread.Sleep(1000);

            var secondDateTime = OnlineTime.GetTime();

            TimeSpan diff = secondDateTime - firstDateTime;

            Assert.IsTrue(diff.TotalMilliseconds > 950 && diff.TotalMilliseconds < 1050);

            // Test caching speed.

            OnlineTime.ClearCache();
            Stopwatch watch = new Stopwatch();
            var firstDateTimeSpeedTest = OnlineTime.GetTime();
            Console.WriteLine($"Ticks without cache: {watch.ElapsedTicks}.");
            watch.Restart();
            var secondDateTimeSpeedTest = OnlineTime.GetTime();
            Console.WriteLine($"Ticks with cache: {watch.ElapsedTicks}.");
        }
    }
}
