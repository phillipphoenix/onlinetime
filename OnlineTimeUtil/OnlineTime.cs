﻿using System;
using System.Threading.Tasks;

//
// The following code found online on Stack Overflow at:
// https://stackoverflow.com/a/44532213
// 

namespace OnlineTimeUtil
{
    public static class OnlineTime
    {

        /// <summary>
        /// Fetches time online (or from cache if enabled (default)).
        /// </summary>
        /// <param name="toLocalTime">Convert date time to local time before returning.</param>
        /// <returns></returns>
        public static DateTime GetTime(bool toLocalTime = false)
        {
            return GetTime(false, toLocalTime);
        }

        /// <summary>
        /// Fetches time online (or from cache if enabled (default)).
        /// Can force an online fetch.
        /// </summary>
        /// <param name="forceFetch">If true, forces an online fetch.</param>
        /// <param name="toLocalTime">Converts date time to local time before returning.</param>
        /// <returns></returns>
        public static DateTime GetTime(bool forceFetch, bool toLocalTime = false)
        {

            bool useCache = OnlineTimeSettings.UseCache;

            DateTime dateTime = useCache ? Cache.FetchTime() : Fetcher.FetchTime();

            if (!toLocalTime)
            {
                return dateTime;
            }
            return dateTime.ToLocalTime(); // without ToLocalTime() = faster.
        }

        /// <summary>
        /// Syncs the cache with the online date time, so next fetch from cache will use an updated value.
        /// </summary>
        /// <exception cref="OnlineTimeSettingsException">Thrown if cache is not enabled.</exception>
        public static void SyncCache()
        {
            bool useCache = OnlineTimeSettings.UseCache;

            if (!useCache)
            {
                throw new OnlineTimeSettingsException("Can't sync online time cache, when cache is disabled in online time settings. Set UseCache to true (default) to use cache features.");
            }

            Cache.Sync();
        }

        /// <summary>
        /// Syncs the cache with the online date time, so next fetch from cache will use an updated value, but does so asynchronously.
        /// </summary>
        /// <exception cref="OnlineTimeSettingsException">Thrown if cache is not enabled.</exception>
        public static Task SyncCacheAsync()
        {
            bool useCache = OnlineTimeSettings.UseCache;

            if (!useCache)
            {
                throw new OnlineTimeSettingsException("Can't sync online time cache, when cache is disabled in online time settings. Set UseCache to true (default) to use cache features.");
            }

            return Cache.SyncAsync();
        }

        /// <summary>
        /// Clears the cache so next fetch is forced to fetch from online.
        /// </summary>
        public static void ClearCache()
        {
            bool useCache = OnlineTimeSettings.UseCache;

            if (!useCache)
            {
                throw new OnlineTimeSettingsException("Can't sync online time cache, when cache is disabled in online time settings. Set UseCache to true (default) to use cache features.");
            }

            Cache.Clear();
        }
    }
}