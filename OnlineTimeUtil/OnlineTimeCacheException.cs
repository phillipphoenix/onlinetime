﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineTimeUtil
{
    public class OnlineTimeCacheException : Exception
    {

        public OnlineTimeCacheException(string message) : base(message) { }

        public OnlineTimeCacheException(string message, Exception innerException) : base(message, innerException) { }

    }
}
