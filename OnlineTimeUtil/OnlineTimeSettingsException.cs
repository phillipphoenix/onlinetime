﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineTimeUtil
{
    public class OnlineTimeSettingsException : Exception
    {
        
        public OnlineTimeSettingsException(string message) : base(message) { }

        public OnlineTimeSettingsException(string message, Exception innerException) : base(message, innerException) { }

    }
}
