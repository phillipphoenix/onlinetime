﻿
namespace OnlineTimeUtil
{
    /// <summary>
    /// Used to setup the settings used within the online time system.
    /// </summary>
    public static class OnlineTimeSettings
    {
        public static SettingsContainer FluidSetter { get; private set; } = new SettingsContainer();

        /// <summary>
        /// The method with which to fetch the online time.
        /// </summary>
        public static FetchMethod FetchMethod { get; set; } = FetchMethod.Ntp;
        /// <summary>
        /// The NTP server to ask the time from. Default is 'time.google.com'. Used when using the NTP method (see <see cref="FetchMethod"/>).
        /// </summary>
        public static string NtpServerUrl { get; set; } = "time.google.com";
        /// <summary>
        /// The url to the API endpoint. No default, but is required.
        /// </summary>
        public static string HttpEndpoint { get; set; }
        /// <summary>
        /// The HTTP Method to use for fetching the datetime.
        /// </summary>
        public static HttpMethod HttpMethod { get; set; } = HttpMethod.Get;
        /// <summary>
        /// Set if the server supports requesting the datetime in specific format. (See <see cref="HttpFormatPropertyName"/> for setting format name and <see cref="HttpDateTimeFormat"/> for setting format).
        /// </summary>
        public static bool HttpSupportsFormatRequest { get; set; } = true;
        /// <summary>
        /// The name of the format property, when requesting time (if the server supports sending a format along). See <see cref="HttpSupportsFormatRequest"/> to enable/disable. Default is "format".
        /// </summary>
        public static string HttpFormatPropertyName { get; set; } = "format";
        /// <summary>
        /// The format in which the API returns the date time. (default: "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"). Will also be used in format request if enabled (see <see cref="HttpSupportsFormatRequest"/>).
        /// </summary>
        public static string HttpDateTimeFormat { get; set; } = "YYYY-MM-DDTHH:mm:ss.SSSZ";
        /// <summary>
        /// If false the return value is expected to be a string, else a JSON object is expected. 
        /// </summary>
        public static bool HttpReturnsJson { get; set; } = false;
        /// <summary>
        /// If the API returns a JSON object this is the path to the datetime property. Should be separated with '.'. Is required, if JSON object is returned.
        /// </summary>
        public static string HttpJsonPath { get; set; }
        /// <summary>
        /// If true (cache is enabled) the time will be cached and a new online time will only be fetched,
        /// when forceFetch is true in fetch methods or <see cref="OnlineTime.SyncCache"/> or <see cref="OnlineTime.SyncCacheAsync"/> is called. Default is true.
        /// </summary>
        public static bool UseCache { get; set; } = true;
        /// <summary>
        /// How many attempts to make to the server before declaring that there is no connection. Default is 5.
        /// </summary>
        public static int MaxFetchAttempts { get; set; } = 5;

        /// <summary>
        /// Revert settings back to default.
        /// </summary>
        public static void RevertToDefaultSettings()
        {
            FetchMethod = FetchMethod.Ntp;
            NtpServerUrl = "time.google.com";
            HttpEndpoint = null;
            HttpMethod = HttpMethod.Get;
            HttpSupportsFormatRequest = true;
            HttpFormatPropertyName = "format";
            HttpDateTimeFormat = @"yyyy-MM-dd\THH:mm:ss.fffzzz";
            HttpReturnsJson = false;
            HttpJsonPath = null;
            UseCache = true;
            MaxFetchAttempts = 5;
        }

        public class SettingsContainer
        {   

            /// <summary>
            /// The method with which to fetch the online time.
            /// </summary>
            public SettingsContainer SetFetchMethod(FetchMethod fetchMethod)
            {
                FetchMethod = fetchMethod;
                return this;
            }

            /// <summary>
            /// The NTP server to ask the time from. Default is 'time.google.com'.
            /// </summary>
            public SettingsContainer SetNtpServerUrl(string ntpServerUrl)
            {
                NtpServerUrl = ntpServerUrl;
                return this;
            }

            /// <summary>
            /// The url to the API endpoint. No default, but is required.
            /// </summary>
            public SettingsContainer SetHttpEndpoint(string httpEndpoint)
            {
                HttpEndpoint = httpEndpoint;
                return this;
            }

            /// <summary>
            /// The HTTP Method to use for fetching the datetime.
            /// </summary>
            public SettingsContainer SetHttpMethod(HttpMethod httpMethod)
            {
                HttpMethod = httpMethod;
                return this;
            }

            /// <summary>
            /// Set if the server supports requesting the datetime in specific format. (See <see cref="HttpFormatPropertyName"/> for setting format name and <see cref="HttpDateTimeFormat"/> for setting format).
            /// </summary>
            public SettingsContainer SetHttpSupportsFormatRequest(bool isFormatRequestSupported)
            {
                HttpSupportsFormatRequest = isFormatRequestSupported;
                return this;
            }

            /// <summary>
            /// The name of the format property, when requesting time (if the server supports sending a format along). Default is "format".
            /// </summary>
            public SettingsContainer SetHttpFormatName(string formatPropertyName)
            {
                HttpFormatPropertyName = formatPropertyName;
                return this;
            }

            /// <summary>
            /// The format in which the API returns the date time. (default: "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]").
            /// </summary>
            public SettingsContainer SetHttpDateTimeFormat(string format)
            {
                HttpDateTimeFormat = format;
                return this;
            }

            /// <summary>
            /// If false the return value is expected to be a string, else a JSON object is expected. 
            /// </summary>
            public SettingsContainer SetHttpReturnsJson(bool isReturningJsonObject)
            {
                HttpReturnsJson = isReturningJsonObject;
                return this;
            }

            /// <summary>
            /// If the API returns a JSON object this is the path to the datetime property. Should be separated with '.'. Is required, if JSON object is returned.
            /// </summary>
            public SettingsContainer SetHttpJsonPath(string jsonPath)
            {
                HttpJsonPath = jsonPath;
                return this;
            }

            /// <summary>
            /// If true (cache is enabled) the time will be cached and a new online time will only be fetched,
            /// when forceFetch is true in fetch methods or <see cref="OnlineTime.SyncCache"/> or <see cref="OnlineTime.SyncCacheAsync"/> is called. Default is true.
            /// </summary>
            public SettingsContainer SetUseCache(bool useCache)
            {
                UseCache = useCache;
                return this;
            }

            /// <summary>
            /// How many attempts to make to the server before declaring that there is no connection. Default is 5.
            /// </summary>
            public SettingsContainer SetMaxFetchAttempts(int maxAttempts)
            {
                MaxFetchAttempts = maxAttempts;
                return this;
            }

        }
    }

    public enum FetchMethod
    {
        Ntp,
        Http,
    }

    public enum HttpMethod
    {
        Get,
        Post,
    }

}
